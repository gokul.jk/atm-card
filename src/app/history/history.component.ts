import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
})
export class HistoryComponent implements OnInit {
  todo: any;
  Withdrawal: any;
  Balance: any;
  constructor(private http: HttpClient) {}
  ngOnInit() {
    this.http
      .get('https://sample-23f2f-default-rtdb.firebaseio.com/Bank.json')
      .subscribe((Response) => {
        this.todo = Response;
      });
    this.http
      .get('https://sample-23f2f-default-rtdb.firebaseio.com/Withdrawal.json')
      .subscribe((Response) => {
        this.Withdrawal = Response;
      });
    this.http
      .get('https://sample-23f2f-default-rtdb.firebaseio.com/Balance.json')
      .subscribe((Response) => {
        this.Balance = Response;
      });
  }
}
